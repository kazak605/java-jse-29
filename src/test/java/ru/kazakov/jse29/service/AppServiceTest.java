package ru.kazakov.jse29.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AppServiceTest {
    private AppService appService;

    @BeforeEach
    public void setMyService() {
        appService = new AppService();
    }

    @Test
    void sum() {
        assertEquals(4, appService.sum("1", "3"));
    }

    @Test
    void sumException() {
        assertThrows(IllegalArgumentException.class, () -> appService.sum("0.1", "1"));
    }

    @Test
    void factorial() {
        assertEquals(24, appService.factorial("4"));
    }

    @Test
    void factorialZero() {
        assertEquals(1, appService.factorial("0"));
    }

    @Test
    void factorialExceptionWrongSymbol() {
        assertThrows(IllegalArgumentException.class, () -> appService.factorial("3b"));
    }

    @Test
    void factorialExceptionNegativeNumber() {
        assertThrows(IllegalArgumentException.class, () -> appService.factorial("-2"));
    }

    @Test
    void factorialExceptionTooBig() {
        assertThrows(IllegalArgumentException.class, () -> appService.factorial("234567653"));
    }

    @Test
    void fibonacci() {
        long[] expected = {0, 1, 1, 2, 3, 5};
        long[] result = appService.fibonacci("5");
        assertArrayEquals(expected, result);
    }

    @Test
    void fibonacciZero() {
        long[] expected = {0};
        long[] result = appService.fibonacci("0");
        assertArrayEquals(expected, result);
    }

    @Test
    void fibonacciFirstNumber() {
        long[] expected = {0, 1};
        long[] result = appService.fibonacci("1");
        assertArrayEquals(expected, result);
    }

    @Test
    void fibonacciExceptionWrongSymbol() {
        assertThrows(IllegalArgumentException.class, () -> appService.fibonacci("3b"));
    }

    @Test
    void fibonacciExceptionNegativeNumber() {
        assertThrows(IllegalArgumentException.class, () -> appService.fibonacci("-2"));
    }
}