package ru.kazakov.jse29.constant;

public class TerminalConst {

    public static final String HELP = "help";
    public static final String EXIT = "exit";

    public static final String SUM = "sum";
    public static final String FACTORIAL = "factorial";
    public static final String FIBONACCI = "fibonacci";

}
