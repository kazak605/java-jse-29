package ru.kazakov.jse29.service;

import static java.lang.Long.*;

public class AppService {

    public long sum(String arg1, String arg2) {
        return toLong(arg1) + toLong(arg2);
    }

    public long factorial(String arg) {
        long result = 1;
        long argLong = toLong(arg);
        int factor;
        isPositive(argLong);
        if (argLong == 0) {
            return 1;
        }
        for (factor = 1; factor <= argLong; factor++) {
            result *= factor;
        }
        return result;
    }

    public long[] fibonacci(String arg) {
        long argLong = toLong(arg);
        isPositive(argLong);
        long[] result = new long[Integer.parseInt(arg) + 1];
        if (argLong == 0) {
            result[0] = 0;
            return result;
        }
        if (argLong == 1) {
            result[0] = 0;
            result[1] = 1;
            return result;
        }
        result[0] = 0;
        result[1] = 1;
        for (int i = 2; i <= argLong; i++) {
            result[i] = result[i - 1] + result[i - 2];
        }
        return result;
    }

    private long toLong(String arg) {
        try {
            return parseLong(arg);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("[CONVERSION ERROR!]");
        }
    }

    private void isPositive(long arg) {
        if ((arg < 0) || (arg > 20))
            throw new IllegalArgumentException("[THE ARGUMENT IS NEGATIVE OR CAUSES A LONG OVERFLOW!]");
    }

}

