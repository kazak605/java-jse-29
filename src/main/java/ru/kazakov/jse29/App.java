package ru.kazakov.jse29;

import ru.kazakov.jse29.controller.AppController;
import ru.kazakov.jse29.service.AppService;

import java.util.Scanner;

import static ru.kazakov.jse29.constant.TerminalConst.*;

public class App {

    AppService appService = new AppService();

    AppController appController = new AppController(appService);

    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        App app = new App();
        app.run(args);
        app.appController.displayWelcome();
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            app.run(command);
        }
    }

    public void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        run(param);
    }

    public void run(String arg) {
        if (arg == null) return;
        switch (arg) {
            case HELP:
                appController.displayHelp();
                return;
            case EXIT:
                appController.displayExit();
                return;
            case SUM:
                appController.sum();
                return;
            case FACTORIAL:
                appController.factorial();
                return;
            case FIBONACCI:
                appController.fibonacci();
                return;
            default:
                appController.displayError();
        }
    }

}
