package ru.kazakov.jse29.controller;

import ru.kazakov.jse29.service.AppService;

import java.util.Scanner;
import java.util.logging.Logger;

public class AppController {

    final Scanner scanner = new Scanner(System.in);

    static Logger logger = Logger.getLogger(AppController.class.getName());

    AppService appService;

    public AppController(AppService appService) {
        this.appService = appService;
    }

    public void displayExit() {
        logger.info("Terminate program...");
    }

    public void displayError() {
        logger.info("Error! Unknown program argument...");
    }

    public void displayWelcome() {
        logger.info("** WELCOME TO JSE-29 **");
    }

    public void displayHelp() {
        logger.info("help - Display list of commands.");
        logger.info("exit - Terminate console application.");
        logger.info("");
        logger.info("sum - Sum of two arguments.");
        logger.info("factorial - Factorial-a function defined on a set of non-negative integers.");
        logger.info("fibonacci - Elements of a numerical sequence.");
    }

    public void sum() {
        logger.info("[SUM OF TWO ARGUMENTS]");
        logger.info("[PLEASE, ENTER FIRST ARGUMENT]");
        final String arg1 = scanner.nextLine();
        logger.info("[PLEASE, ENTER SECOND ARGUMENT]");
        final String arg2 = scanner.nextLine();
        long result = appService.sum(arg1, arg2);
        System.out.println(result);
    }

    public void factorial() {
        logger.info("[PLEASE, ENTER ARGUMENT]");
        final String arg = scanner.nextLine();
        final long result = appService.factorial(arg);
        System.out.println(result);
    }

    public void fibonacci() {
        logger.info("[PLEASE, ENTER ARGUMENT]");
        final String arg = scanner.nextLine();
        long[] result = appService.fibonacci(arg);
        for (Long longs : result) {
            System.out.print(longs + " ");
        }
    }

}
